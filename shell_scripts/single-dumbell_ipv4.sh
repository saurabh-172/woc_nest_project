sudo ip netns add red
sudo ip netns add blue
sudo ip netns add r1
sudo ip netns add r2
sudo ip link add eth_red type veth peer name eth_r1_red
sudo ip link add eth_blue type veth peer name eth_r2_blue
sudo ip link add eth_r1_r2 type veth peer name eth_r2_r1
sudo ip link set eth_red netns red
sudo ip link set eth_blue netns blue
sudo ip link set eth_r1_r2 netns r1
sudo ip link set eth_r1_red netns r1
sudo ip link set eth_r2_blue netns r2
sudo ip link set eth_r2_r1 netns r2
sudo ip netns exec red ip address add 10.0.1.1/24 dev eth_red
sudo ip netns exec r1 ip address add 10.0.1.2/24 dev eth_r1_red
sudo ip netns exec r1 ip address add 10.0.2.1/24 dev eth_r1_r2
sudo ip netns exec r2 ip address add 10.0.2.2/24 dev eth_r2_r1
sudo ip netns exec r2 ip address add 10.0.3.1/24 dev eth_r2_blue
sudo ip netns exec blue ip address add 10.0.3.2/24 dev eth_blue
sudo ip netns exec red ip link set eth_red up
sudo ip netns exec blue ip link set eth_blue up
sudo ip netns exec r1 ip link set eth_r1_red up
sudo ip netns exec r1 ip link set eth_r1_r2 up
sudo ip netns exec r2 ip link set eth_r2_blue up
sudo ip netns exec r2 ip link set eth_r2_r1 up
sudo ip netns exec r1 sysctl -w net.ipv4.ip_forward=1
sudo ip netns exec r2 sysctl -w net.ipv4.ip_forward=1
sudo ip netns exec red ip route add default via 10.0.1.2 dev eth_red
sudo ip netns exec blue ip route add default via 10.0.3.1 dev eth_blue
sudo ip netns exec r1 ip route add default via 10.0.2.2 dev eth_r1_r2
sudo ip netns exec r2 ip route add default via 10.0.2.1 dev eth_r2_r1
sudo ip netns exec red ping -c5 10.0.3.2
sudo ip netns del red
sudo ip netns del blue
sudo ip netns del r1
sudo ip netns del r2
