sudo ip netns add red
sudo ip netns add blue
sudo ip netns add router
sudo ip link add eth_red type veth peer name eth_router_red
sudo ip link add eth_blue type veth peer name eth_router_blue
sudo ip link set eth_red netns red
sudo ip link set eth_blue netns blue
sudo ip link set eth_router_red netns router
sudo ip link set eth_router_blue netns router
sudo ip netns exec red ip link set eth_red up
sudo ip netns exec blue ip link set eth_blue up
sudo ip netns exec red ip address add 10.1.1.1/24 dev eth_red
sudo ip netns exec blue ip address add 10.1.2.2/24 dev eth_blue
sudo ip netns exec router ip link set eth_router_red up
sudo ip netns exec router ip link set eth_router_blue up
sudo ip netns exec router ip address add 10.1.1.2/24 dev eth_router_red
sudo ip netns exec router ip address add 10.1.2.1/24 dev eth_router_blue
sudo ip netns exec router sysctl -w net.ipv4.ip_forward=1
sudo ip netns exec red ip route add default via 10.1.1.2 dev eth_red
sudo ip netns exec blue ip route add default via 10.1.2.1 dev eth_blue
sudo ip netns exec red ping -c3 10.1.2.2
sudo ip netns del red
sudo ip netns del blue
sudo ip netns del router